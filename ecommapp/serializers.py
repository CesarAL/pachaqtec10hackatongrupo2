from .models import Cupon, Estado, Categoria, Producto, Pedido
from django.contrib.auth.models import User
from rest_framework import serializers


class CuponSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cupon
        fields = ['id', 'codigo', 'descripcion', 'descuento']


class EstadoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Estado
        fields = ['id', 'descripcion']

class ClienteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email']

class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Categoria
        fields = ['id','nombre', 'descripcion']

class ProductoSerializer(serializers.ModelSerializer):
    #id = serializers.HyperlinkedIdentityField(view_name="categoria-detail")
    categoria = CategoriaSerializer()
    class Meta:
        model = Producto
        fields = ['id', 'nombre', 'descripcion', 'categoria', 'igv', 'imagen', 'imagen_card', 'precio', 'descuento']



class PedidoSerializer(serializers.HyperlinkedModelSerializer):
    cliente = ClienteSerializer()
    estado = EstadoSerializer()
    cupon = CuponSerializer()
    detallePedido = ProductoSerializer(many=True)
    class Meta:
        model = Pedido
        fields = ['id','fecha', 'subtotal', 'igv', 'total', 'cliente', 'estado', 'cupon', 'detallePedido']