from django.contrib import admin
from .models import Cupon, Categoria,  Pedido, Producto, Estado
# Register your models here.
admin.site.register(Cupon)
admin.site.register(Categoria)
admin.site.register(Pedido)
admin.site.register(Producto)
admin.site.register(Estado)