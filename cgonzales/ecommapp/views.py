from rest_framework import viewsets
from django.contrib.auth.models import User
from .models import Cupon, Estado, Pedido, Producto, Categoria
from .serializers import CuponSerializer, EstadoSerializer, ClienteSerializer, ProductoSerializer, CategoriaSerializer, PedidoSerializer
from rest_framework import permissions

from rest_framework import generics
import django_filters

from django.views.decorators.csrf import csrf_exempt
import requests
from django.http import JsonResponse
from .template import payment
from django.shortcuts import render

# Create your views here.

class CuponFilter(django_filters.FilterSet):
   class Meta:
       model = Cupon
       fields = {
           'codigo': ['startswith'],
           
       }
       together = ['codigo']

class CuponViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Cupon.objects.get_queryset().order_by('id')
    serializer_class = CuponSerializer
    filter_class = CuponFilter

class EstadoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Estado.objects.get_queryset().order_by('id')
    serializer_class = EstadoSerializer


class CategoriaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Categoria.objects.get_queryset().order_by('id')
    serializer_class = CategoriaSerializer


class ClienteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = User.objects.get_queryset().order_by('id')
    serializer_class = ClienteSerializer
   # permission_classes = [permissions.IsAuthenticated]


class ProductoFilter(django_filters.FilterSet):
    class Meta:
       model = Producto
       fields = {
           'nombre': ['startswith'],
           'precio': ['startswith'],
       }
       together = ['nombre', 'precio']


class ProdutoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Producto.objects.get_queryset().order_by('id')
    serializer_class = ProductoSerializer
    filter_class = ProductoFilter
    
    #permission_classes = [permissions.IsAuthenticated]
# Equivalent FilterSet:

class PedidoFilter(django_filters.FilterSet):
    cliente = django_filters.ModelChoiceFilter(queryset=User.objects.all())
    estado = django_filters.ModelChoiceFilter(queryset=Estado.objects.all())
    class Meta:
       model = Pedido
       fields = ['cliente', 'estado']

class PedidoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Pedido.objects.all()
    serializer_class = PedidoSerializer
    filter_class = PedidoFilter

def payment(request):
    return render(request, 'payment/index.html')


@csrf_exempt
def charges(request):
    if request.method == 'POST':
        token = request.POST['token']
        installments = request.POST['installments']
        pedido = int(request.POST['idPedido'])
        email = request.POST['email']
        monto = int(request.POST['monto'])
        descrpcion = 'Pago pachaqtec curso online'
        moneda = request.POST['moneda']
        auth_token = 'sk_test_SWyklAB8rIyjXmje'
        hed = {'Authorization': 'Bearer ' + auth_token}
        data = {
            'amount': monto,
            'currency_code': moneda,
            'email': email,
            'source_id': token,
            'installments': installments,
            'metadata': {'Descripcion': descrpcion}
        }
        url = 'https://api.culqi.com/v2/charges'
        charge = requests.post(url, json=data, headers=hed)

        print(charge)
        dicRes = {'message': 'EXITO'}
        objPedido = Pedido.objects.get(id=pedido)
        objPedido.estado = 2
        objPedido.save()
        return JsonResponse(charge.json(), safe=False)

    return JsonResponse("only POST method", safe=False)
